<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;
    
    protected $fillable = ['title', 'summary', 'harvard_id'];
    
    protected $with = [
        'authors',
        'genres'
    ];
    
    public function authors()
    {
        return $this->hasMany(Author::class);
    }
    
    public function genres()
    {
        return $this->hasMany(Genre::class);
    }
    
    
}
