<?php

namespace App\Console\Model;


/**
 * Simple data transfer class.
 */
class AuthorDto
{
    
    /**
     *
     * @var string
     */
    private $name;
    
    /**
     * 
     * @param string $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }
    
    
    public function getName() {
        return $this->name;
    }

}
