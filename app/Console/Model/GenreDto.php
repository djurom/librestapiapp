<?php

namespace App\Console\Model;


/**
 * Simple data transfer class.
 */
class GenreDto
{
    
    /**
     *
     * @var string
     */
    private $genre;
    
    
    public function __construct($genre)
    {
        $this->genre = $genre;
    }
    
    
    public function getGenre() {
        return $this->genre;
    }

}
