<?php

namespace App\Console\Model;

/**
 * Simple data transfer class, representing the book item retrieved from Harvard API.
 */
class BookItemDto {
    
    
    /**
     *
     * @var string
     */
    private $title;
    
    /**
     *
     * @var AuthorDto[]
     */
    private $authors;
    
    /**
     *
     * @var string
     */
    private $summary;
    
    /**
     *
     * @var string
     */
    private $harvardId;
    
    /**
     *
     * @var GenreDto[]
     */
    private $genres;
    
    /**
     * 
     * @param string $title
     */
    public function __construct(string $title)
    {
        $this->title = $title;
    }


    public function setAuthors(array $authors) {
        $this->authors = $authors;
        return $this;
    }

    public function setSummary($summary) {
        $this->summary = $summary;
        return $this;
    }

    public function setHarvardId($harvardId) {
        $this->harvardId = $harvardId;
        return $this;
    }

    public function setGenres(array $genres) {
        $this->genres = $genres;
        return $this;
    }

    public function getTitle() {
        return $this->title;
    }
    
    public function getAuthors(): array 
    {
        return $this->authors;
    }
    
    public function getSummary() {
        return $this->summary;
    }

    public function getHarvardId() {
        return $this->harvardId;
    }
    
    public function getGenres(): array {
        return $this->genres;
    }

}
