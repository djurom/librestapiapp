<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;


use App\Services\Harvard\PullDataRestApiService;
use App\Services\Harvard\ParseResponseService;
use App\Services\BooksService;


class PullLibraryDataCommand extends Command
{
    
    const OPTION_ERROR = 'Command options invalid. "--name" and/or "--genre" is required.';
    
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'librestapiapp:pull_library_data {--name=} {--genre=} {--start=0} {--limit=100}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pulls library items data from Harvard Library Rest API server. At least one option is required (name or genre). Both can be used combined.';

    /**
     *
     * @var PullDataRestApiService
     */
    private $pullDataRestApiService;
    
    /**
     *
     * @var ParseResponseService
     */
    private $parseResponseService;
    
    /**
     *
     * @var BooksService
     */
    private $booksService;
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(PullDataRestApiService $pullDataRestApiService, 
            ParseResponseService $parseResponseService, BooksService $booksService)
    {
        parent::__construct();
        $this->pullDataRestApiService = $pullDataRestApiService;
        $this->parseResponseService = $parseResponseService;
        $this->booksService = $booksService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = $this->option('name');
        $genre = $this->option('genre');
        
        $start = $this->option('start');
        $limit = $this->option('limit');
        
        if(true === $this->validateOptions($name, $genre)) {
            $response = $this->pullDataRestApiService->getItems($name, $genre, $start, $limit);
            
            $bookItems = $this->parseResponseService->parseResponse($response);
            
            $this->booksService->transformBooksDataTransferObjects($bookItems);
            
        } else {
            $this->output->error(self::OPTION_ERROR);
        }
    }
    
    /**
     * 
     * @param string $field
     * @param string $text
     * @return bool
     */
    private function validateOptions($field, $text):bool
    {
        if(!empty($field) || !empty($text))
            return true;
        else 
            return false;
            
    }
}
