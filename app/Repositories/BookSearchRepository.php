<?php

namespace App\Repositories;

use App\Models\Book;



class BookSearchRepository 
{
    
    /**
     * 
     * @param string $title
     * @param string $name
     * @param string $genre
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function findByTitleNameAndGenre(string $title, string $name, string $genre)
    {
        return Book::where('title','LIKE',"%{$title}%")
          ->whereHas('genres', function($q) use($genre) {
            $q->where('genre','LIKE', "%{$genre}%");
        })->whereHas('authors', function($q) use($name) {
            $q->where('name','LIKE', "%{$name}%");
        })->get();
    }
    
    /**
     * 
     * @param string $title
     * @param string $genre
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function findByTitleAndGenre(string $title, string $genre)
    {
        return Book::where('title','LIKE',"%{$title}%")
            ->whereHas('genres', function($q) use($genre) {
                $q->where('genre','LIKE', "%{$genre}%");
        })->get();
    }
    
    /**
     * 
     * @param string $title
     * @param string $name
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function findByTitleAndName(string $title, string $name)
    {
        return Book::where('title','LIKE',"%{$title}%")
            ->whereHas('authors', function($q) use($name) {
                $q->where('name','LIKE', "%{$name}%");
        })->get();
    }
    
    /**
     * 
     * @param string $name
     * @param string $genre
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function findByNameAndGenre(string $name, string $genre)
    {
        return Book::whereHas('genres', function($q) use($genre) {
            $q->where('genre','LIKE', "%{$genre}%");
        })->whereHas('authors', function($q) use($name) {
            $q->where('name','LIKE', "%{$name}%");
        })->get();
    }
    
    /**
     * 
     * @param string $genre
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function findByGenre(string $genre)
    {
        return Book::whereHas('genres', function($q) use($genre) {
            $q->where('genre','LIKE', "%{$genre}%");
        })->get();
    }
    
    /**
     * 
     * @param string $name
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function findByName(string $name)
    {   
        return Book::whereHas('authors', function($q) use($name) {
            $q->where('name','LIKE', "%{$name}%");
        })->get();

    }
    
    /**
     * 
     * @param string $title
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function findByTitle(string $title)
    {
        $books = Book::where('title', 'LIKE', "%{$title}%")->get();
        return $books;
    }
    
    /**
     * 
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function findAll()
    {
        return Book::all();
    }
    
}
