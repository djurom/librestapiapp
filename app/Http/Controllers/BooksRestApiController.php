<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Book;
use Log;
use App\Services\BookSearchService;


class BooksRestApiController extends Controller
{
    
    public function index(Request $request, BookSearchService $searchService)
    {
        $title = $request->query('title');
        $name = $request->query('name');
        $genre = $request->query('genre');
        
        $books = $searchService->resolveSearchMethodAndFind($title, $name, $genre);
        
        return $books;
    }

    public function show(Book $book)
    {
        return $book;
    }
    
    public function josh()
    {
        return ['title'=>'neki lipi naslov','authors'=>['autor1', 'pisac2','shkrabalo3']];
    }

    
}
