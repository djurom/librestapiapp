<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;


class BrowseBooksController extends Controller
{
    
    const PAGE_SIZE = 100;
    
    
    public function listAll(Request $request)
    {
        $page = empty($request->query('page')) ? 1 : $request->query('page');
        
        $booksCount = Book::all()->count();
        $totalPages = ceil($booksCount / self::PAGE_SIZE);
        $offset = ($page-1) * self::PAGE_SIZE;
        return view('books.list_all', [
            'books' => Book::all()->skip($offset)->take(self::PAGE_SIZE),
            'next_page' => route('books.list', ['page'=>$page+1]),
            'previous_page' => route('books.list', ['page'=>$page-1]),
            'page' => $page,
            'total_pages' => $totalPages 
        ]);
    }
}
