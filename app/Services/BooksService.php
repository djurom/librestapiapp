<?php


namespace App\Services;

use App\Models\Book;
use App\Models\Author;
use App\Models\Genre;


class BooksService
{
    /**
     * 
     * @param BookItemDto[] $booksDtoCollection
     */
    public function transformBooksDataTransferObjects(array $booksDtoCollection)
    {
        foreach($booksDtoCollection as $bookDto) {
            
            $existingBook = Book::where('harvard_id', $bookDto->getHarvardId())->first();
            if(!$existingBook instanceof Book) {
                $authors = $this->createAuthors($bookDto->getAuthors());
                $genres = $this->createGenres($bookDto->getGenres());
                $book = new Book(['title' => $bookDto->getTitle(), 'summary' => $bookDto->getSummary(), 'harvard_id' => $bookDto->getHarvardId()]);
                $book->save();
                $book->authors()->saveMany($authors);
                $book->genres()->saveMany($genres);
            }
        }
    }
    
    /**
     * 
     * @param AuthorDto[] $authorDTObjects
     * @return Author[]
     */
    private function createAuthors(array $authorDTObjects):array
    {
        $authors = [];
        foreach($authorDTObjects as $authorDto) {
            $authors[] = new Author(['name' => $authorDto->getName()]);
        }
        return $authors;
    }
    
    /**
     * 
     * @param GenreDto[] $genreDTObjects
     * @return Genre[]
     */
    private function createGenres(array $genreDTObjects)
    {
        $genres = [];
        foreach($genreDTObjects as $genreDto) {
            $genres[] = new Genre(['genre' => ucfirst($genreDto->getGenre())]);
        }
        return $genres;
    }
}
