<?php

namespace App\Services\Harvard;

use Illuminate\Support\Facades\Http;



class PullDataRestApiService
{
    
    const API_URL = 'https://api.lib.harvard.edu/v2/';
    
    const ITEMS_ENDPOINT = 'items.json';
    
    
    /**
     * 
     * @param string $name
     * @param string $genre
     * @param int $start
     * @param int $limit
     * @return string|Exception
     */
    public function getItems($name=null, $genre=null, int $start, int $limit)
    {
        $queryParams = $this->resolveQueryParameters($name, $genre);
        $queryParams['start'] = $start;
        $queryParams['limit'] = $limit;
        
        $response = Http::get(self::API_URL . self::ITEMS_ENDPOINT, [
            'name' => $name,
            'genre' => $genre,
            'start' => $start,
            'limit' => $limit
        ]);
        
        if($response->successful())
            return $response;
        else 
            return $response->throw();
    }
    
    /**
     * 
     * @param string|null $name
     * @param string|null $genre
     * @return string[]
     */
    private function resolveQueryParameters($name, $genre)
    {
        $params = [];
        if(!empty($name)) 
            $params['name'] = $name;
        if(!empty($genre))
            $params['genre'] = $genre;
        
        return $params;
    }
}
