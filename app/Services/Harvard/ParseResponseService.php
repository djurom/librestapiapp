<?php

namespace App\Services\Harvard;

use App\Console\Model\BookItemDto;
use App\Console\Model\AuthorDto;
use App\Console\Model\GenreDto;



class ParseResponseService
{
    const ITEMS = 'items';
    
    const MODS = 'mods';
    
    const TITLE_INFO = 'titleInfo';
    
    const TITLE = 'title';
    
    const SUB_TITLE = 'sub-title';
    
    const NAME_PART = 'namePart';
    
    const NAME = 'name';
    
    const NON_SORT = 'nonSort';
    
    const TYPE = '@type';
    
    const PERSONAL = 'personal';
    
    const RESOURCE_TYPE = 'typeOfResource';
    
    const COLLECTION = '@collection';
    
    const YES = 'yes';
    
    const MANUSCRIPT = '@manuscript';
    
    const ABSTR = 'abstract';
    
    const SUMMARY = 'Summary';
    
    const TEXT = '#text';
    
    const REC_ID = 'recordIdentifier';
    
    const REC_INFO = 'recordInfo';
    
    const GENRE = 'genre';
    
    const TXT = 'text';
    
    
    /**
     * 
     * @param string $response
     * @return BookItemDto[]
     */
    public function parseResponse(string $response)
    {
        $decoded = json_decode($response, true);
        $bookItems = [];
        
        if(is_array($decoded)) {
            $mods = $decoded[self::ITEMS][self::MODS];
            if(is_array($mods) && count($mods)>0) {
                foreach($mods as $item) {
                    $isBook = $this->validateResponseItem($item);
                    if(true === $isBook)
                        $bookItems[] = $this->resolveBookItemDetails($item); 
                } 
            }
        }
        return $bookItems;
    }
    
    /**
     * 
     * @param string[] $item
     * @return bool
     */
    private function validateResponseItem(array $item): bool
    {
        if(isset($item[self::RESOURCE_TYPE][self::COLLECTION]) && $item[self::RESOURCE_TYPE][self::COLLECTION] == self::YES)
            return false;
        elseif(isset($item[self::RESOURCE_TYPE][self::MANUSCRIPT]) && $item[self::RESOURCE_TYPE][self::MANUSCRIPT] == self::YES)
            return false;
        elseif(!isset($item[self::NAME]))
            return false;
        else 
            return true;
    }
    
    /**
     * 
     * @param string[] $item
     * @return BookItemDto
     */
    private function resolveBookItemDetails(array $item): BookItemDto
    {
        $title = $this->resolveTitle($item);
        $authors = $this->resolveAuthor($item);
        $summary = $this->resolveSummary($item);
        $harvardId = $this->resolveHarvardId($item);
        $genres = $this->resolveGenres($item);
        
        $bookItem = new BookItemDto($title);
        $bookItem->setAuthors($authors)
                    ->setSummary($summary)
                    ->setHarvardId($harvardId)
                    ->setGenres($genres);
        
        return $bookItem;
    }
    
    /**
     * 
     * @param string[] $item
     * @return GenreDto[]
     */
    private function resolveGenres(array $item)
    {
        $genreCollection = [];
        if(isset($item[self::GENRE]) && is_array($item[self::GENRE])) {
            foreach($item[self::GENRE] as $genre) {
                if(isset($genre[self::TEXT]))
                    $genreCollection[] = new GenreDto($genre[self::TEXT]);
            }
        }
        $distinctGenres = array_unique($genreCollection, SORT_REGULAR);
        return $distinctGenres;
    }
    
    /**
     * 
     * @param string[] $item
     * @return string
     */
    private function resolveHarvardId(array $item)
    {
        if(isset($item[self::REC_INFO][self::REC_ID][self::TEXT]))
            return $item[self::REC_INFO][self::REC_ID][self::TEXT];
    }
    
    /**
     * 
     * @param string[] $item
     * @return string
     */
    private function resolveSummary(array $item)
    {
        if(isset($item[self::ABSTR][self::TYPE]) && $item[self::ABSTR][self::TYPE]==self::SUMMARY)
            return isset($item[self::ABSTR][self::TEXT])?$item[self::ABSTR][self::TEXT]:null;
    }
    
    /**
     * 
     * @param string[] $item
     * @return AuthorDto[]
     */
    private function resolveAuthor(array $item): array
    {
        if(isset($item[self::NAME])) {
            $name = $item[self::NAME];
        
            if(isset($name[self::NAME_PART]) && !is_array($name[self::NAME_PART]) && !empty($name[self::NAME_PART])) {
                $authorDto = new AuthorDto($name[self::NAME_PART]);
                return [$authorDto];
            }

            if(isset($name[0][self::NAME_PART]) && !is_array($name[0][self::NAME_PART]) && !empty($name[0][self::NAME_PART])) {
                return $this->formatCollectionOfAuthors($name);
            }


            if(isset($name[self::NAME_PART][0]) && !is_array($name[self::NAME_PART][0]) && !empty($name[self::NAME_PART][0])) {
                $authorDto = new AuthorDto($name[self::NAME_PART][0]);
                return [$authorDto];
            }

            if(isset($name[0][self::NAME_PART][0]) && !empty($name[0][self::NAME_PART][0])) {
                $authorDto = new AuthorDto($name[0][self::NAME_PART][0]);
                return [$authorDto];
            }
        } 
    }
    
    /**
     * 
     * @param string[] $name
     * @return AuthorDto[]
     */
    private function formatCollectionOfAuthors(array $name)
    {
        $authors = [];
        $authorCollection = [];
        foreach($name as $nameElement) {
            if(isset($nameElement[self::TYPE])) {
                if($nameElement[self::TYPE] == self::PERSONAL) {
                    if(!is_array($nameElement[self::NAME_PART]))
                        $authors[] = $nameElement[self::NAME_PART];
                }
            } 
        }
        foreach($authors as $author) {
            $authorDto = new AuthorDto($author);
            $authorCollection[] = $authorDto;
        }
        return $authorCollection;
    }
    
    
    /**
     * 
     * @param string[] $item
     * @return string
     */
    private function resolveTitle(array $item):string
    {
        if(isset($item[self::TITLE_INFO])) {
            if(isset($item[self::TITLE_INFO][self::TITLE])) {
                $title = $item[self::TITLE_INFO][self::TITLE];
                if(isset($item[self::TITLE_INFO][self::NON_SORT]))
                    $title = trim($item[self::TITLE_INFO][self::NON_SORT]) . " " . $title;
                return $title;
            } else {
                $titleInfo = $item[self::TITLE_INFO];
                if(is_array($titleInfo)) {
                    if(isset($titleInfo[0][self::NON_SORT]) && isset($titleInfo[0][self::TITLE]))
                        return trim($titleInfo[0][self::NON_SORT]) . " " . $titleInfo[0][self::TITLE];
                    elseif(!isset($titleInfo[0][self::NON_SORT]) && isset($titleInfo[0][self::TITLE]))
                        return $titleInfo[0][self::TITLE];
                } 
            }
        }
    }
    
}
