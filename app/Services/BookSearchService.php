<?php

namespace App\Services;

use App\Repositories\BookSearchRepository;


class BookSearchService
{
    
    /**
     *
     * @var BookSearcRepository
     */
    private $bookSearchRepository;
    
    /**
     * 
     * @param BookSearchRepository $bookSearchRepository
     */
    public function __construct(BookSearchRepository $bookSearchRepository)
    {
        $this->bookSearchRepository = $bookSearchRepository;
    }
    
    /**
     * 
     * @param string $title
     * @param string $name
     * @param string $genre
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function resolveSearchMethodAndFind($title, $name, $genre)
    {
        if(!empty($title) && !empty($name) && !empty($genre))
            return $this->bookSearchRepository->findByTitleNameAndGenre($title, $name, $genre);
        
        if(!empty($title) && !empty($name) && empty($genre))
            return $this->bookSearchRepository->findByTitleAndName($title, $name);
        
        if(!empty($title) && empty($name) && empty($genre))
            return $this->bookSearchRepository->findByTitle($title);
        
        if(empty($title) && empty($name) && !empty($genre))
            return $this->bookSearchRepository->findByGenre($genre);
        
        if(empty($title) && !empty($name) && empty($genre))
            return $this->bookSearchRepository->findByName($name);
        
        if(empty($title) && !empty($name) && !empty($genre))
            return $this->bookSearchRepository->findByNameAndGenre($name, $genre);
        
        if(!empty($title) && empty($name) && !empty($genre))
            return $this->bookSearchRepository->findByTitleAndGenre($title, $genre);
        
        return $this->bookSearchRepository->findAll();
    }
}
