<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>    

        
</head>
<body>

<div class="container">    
@foreach($books as $book)
    {{ $book->id . ". "}}<strong>{{ $book->title }}</strong> <br />
    @foreach($book->authors as $author)
        <span>{{ $author->name}}; </span>
    @endforeach
    <br />
    @foreach($book->genres as $genre)
        <span class="text-info">{{ $genre->genre}}; </span>
    @endforeach
    <br /><br />
@endforeach

<div>
    @if($page > 1)
        <span><a href="{{ $previous_page }}"> Previous </a></span>
    @else
        <span> Previous </span>
    @endif
    
    <strong>{{ $page }}</strong>
    @if($page < $total_pages)
        <span><a href="{{ $next_page }}"> Next </a></span>
    @else
        <span> Next </span>
    @endif
</div>
</div>
</body>
</html>