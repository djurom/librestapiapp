# Laravel based web application for consuming Harvard Library 'items.json' Rest API server endpoint
## Configuring, building and starting Docker containers
* `$ docker-compose -f docker-compose.yml config`
* `$ docker-compose -f docker-compose.yml build`
* `$ docker-compose -f docker-compose.yml up -d`

## Laravel dependencies, file permissions and encryption key
* `$ docker exec [container_id] composer install`
* `$ docker exec [container_id] chown -R www-data:www-data storage/`
* `$ docker exec [container_id] php artisan key:generate`

The app should then be up and running at: `http://0.0.0.0:8080`.

## Database setup

The application needs MySQL database to work with. It is configured in `docker-compose.yml` as name: librestapiapp, user: root, password: librestapiapp.

The working `.env` file is removed from Git ignore list and is available here.

After you create the database, run migrations to create tables:

* `# php artisan migrate`

Relevant DB tables are `books` for pulled items, `authors` and `genres`.


## The console command for pulling Harvard Library data
* `# php artisan librestapiapp:pull_library_data`

Required option is either `--name` or `--genre`. It accepts both for further narrowing the result. 

It also uses `--start` and `--limit`, as offset and limit. Default values are `0` and `100` respectively.

Example:

* `# php artisan librestapiapp:pull_library_data --genre=science+fiction --name=asimov --start=0 --limit=50`


## Rest API endpoints for accessing stored data

* GET `api/books{params?}` Available parameters: `title`,`name`,`genre`.
* GET `api/book/{id}`

The first one returns all imported items, if no query parameters are supplied. Otherwise, it filters the result by provided parameter value.

Example:

* `http://0.0.0.0:8080/api/books?title=sheep&genre=science+fiction`

That would retrieve items filtered by: 'sheep' in title and belonging to Science Fiction genre.


The `api/book/{id}` endpoint, requires the local item's ID as argument, and retrieves the particular item.

Example:

* `http://0.0.0.0:8080/api/book/42`
Retrieves an item having the ID: 42.

## UI for browsing the pulled data

A very simple UI for browsing all items is available on `http://0.0.0.0:8080/books`.

