<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSummaryAndHarvardIdFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('books', function (Blueprint $table) {
            $table->text('summary')->after('title')->nullable();
            $table->string('harvard_id')->after('summary');
            $table->dropColumn('subtitle');
            $table->dropColumn('author');
            $table->dropColumn('genre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('books', function (Blueprint $table) {
            $table->dropColumn('summary');
            $table->dropColumn('harvard_id');
            $table->string('subtitle');
            $table->string('author');
            $table->string('genre');
            
        });
    }
}
